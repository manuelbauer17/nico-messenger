import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, public afAuth: AngularFireAuth) {

  }

  logout(){
    this.afAuth.auth.signOut().then(success => {
      this.navCtrl.setRoot(LoginPage);
    });
  }
}
