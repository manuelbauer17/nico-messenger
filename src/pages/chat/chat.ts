import { ChatsProvider } from './../../providers/chats/chats';
import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage implements OnDestroy {

  contact: any;
  sendMessage: string = "";
  messages = [];

  private chatSubscription: Subscription;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    private chatProvider: ChatsProvider) {
    this.contact = this.navParams.get('contact');
  }

  ionViewWillEnter(){
    this.loadMessages();
  }

  async loadMessages(){
    this.messages = [];
    let that = this;
    let snapshot = await this.db.collection('users').doc(this.auth.auth.currentUser.uid).ref.get();
    let data = snapshot.data();
    let chatId: string;
    data.chats.forEach(chat => {
      if (chat.participants.indexOf(that.contact.id) !== -1 && chat.participants.indexOf(this.auth.auth.currentUser.uid) !== -1) {
        chatId = chat.chatId;
      }
    });

    if (chatId) {
      this.chatSubscription = this.chatProvider.getChat(chatId).subscribe(chat => {
        that.messages = chat.messages;
      });
    }
  }

  async send(){
    if(this.sendMessage.length > 0){
      let that = this;
      let otherSnap = await this.db.collection('users').doc(this.contact.id).ref.get();
      let otherSnapData = otherSnap.data();
      otherSnapData.chats.forEach(chat => {
        if(chat.participants.indexOf(that.contact.id) !== -1 && chat.participants.indexOf(this.auth.auth.currentUser.uid) !== -1){
          chat.messages.push({
            message: that.sendMessage,
            sender: this.auth.auth.currentUser.uid
          });
          this.db.collection('users').doc(that.contact.id).set(otherSnapData);
        }
      });

      let mySnap = await this.db.collection('users').doc(this.auth.auth.currentUser.uid).ref.get();
      let mySnapData = mySnap.data();
      mySnapData.chats.forEach(chat => {
        if(chat.participants.indexOf(that.contact.id) !== -1 && chat.participants.indexOf(this.auth.auth.currentUser.uid) !== -1){
          chat.messages.push({
            message: that.sendMessage,
            sender: this.auth.auth.currentUser.uid
          });
          this.db.collection('users').doc(this.auth.auth.currentUser.uid).set(mySnapData);
        }
      });
      this.sendMessage = "";
    }
  }

  ngOnDestroy() {
    if (this.chatSubscription) {
      this.chatSubscription.unsubscribe();
    }
  }

}
