import { AngularFirestore } from '@angular/fire/firestore';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  contacts = [];

  constructor(public navCtrl: NavController, public db: AngularFirestore, public auth: AngularFireAuth) {

  }

  ionViewWillEnter(){
    this.getData();
  }

  async getData(){
    this.contacts = [];
    let snapshot = await this.db.collection('meta').doc('allUsers').ref.get();
    let data = snapshot.data().ids;
    for(var i = 0; i < data.length; i++){
      this.db.collection('users').doc(data[i]).ref.get().then(success => {
        if(success.data().id != this.auth.auth.currentUser.uid){
          this.contacts.push(success.data());
        }
      }).catch(error => {
        console.log(error);
      })
    }
  }

  async openChat(contact: any){
    let snapshot = await this.db.collection('users').doc(this.auth.auth.currentUser.uid).ref.get();
    let data = snapshot.data();
    let isAlreadyChatting = false;
    let chatRef = await this.db.collection('users').doc(this.auth.auth.currentUser.uid).ref.get();
    let chatRefData = chatRef.data();
    await chatRefData.chats.forEach(chat => {
      chat.participants.forEach(part => {
        if(part == contact.id){
          isAlreadyChatting = true;
        }
      });
    });

    if(!isAlreadyChatting){
      data.chats.push({
        chatId: this.db.createId(),
        participants: [
          this.auth.auth.currentUser.uid,
          contact.id
        ],
        messages: []
      });
      await this.db.collection('users').doc(this.auth.auth.currentUser.uid).set(data).catch(error => {
        console.log(error);
      });
      let otherUserSnap = await this.db.collection('users').doc(contact.id).ref.get();
      let otherUserData = otherUserSnap.data();
      otherUserData.chats.push({
        chatId: this.db.createId(),
        participants: [
          this.auth.auth.currentUser.uid,
          contact.id
        ],
        messages: []
      });
      await this.db.collection('users').doc(contact.id).set(otherUserData).catch(error => {
        console.log(error);
      });
    } 
    this.navCtrl.push('ChatPage', { contact: contact });
  }
}
