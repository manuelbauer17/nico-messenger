import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  chats = [];

  constructor(public navCtrl: NavController, public db: AngularFirestore, public auth: AngularFireAuth) {}

  ionViewWillEnter(){
    this.loadChats();
  }

  async loadChats(){
    this.chats = [];
    let snapshot = await this.db.collection('users').doc(this.auth.auth.currentUser.uid).ref.get();
    let data = snapshot.data();
    data.chats.forEach(chat => {
      let otherDude = '';
      for(var i = 0; i < chat.participants.length; i++){
        if(chat.participants[i] != this.auth.auth.currentUser.uid){
          otherDude = chat.participants[i];
        }
      }
      this.db.collection('users').doc(otherDude).ref.get().then((success: any) => {
        this.chats.push(success.data());
      }).catch(error => {
        console.log(error);
      });
    });
  }

  openChat(chat: any){
    this.navCtrl.push('ChatPage', { contact: chat });
  }
}
