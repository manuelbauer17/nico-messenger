import { AngularFirestore } from '@angular/fire/firestore';
import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public userName: string = "";
  public loginMail: string = "";
  public loginPassword: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public db: AngularFirestore) {
  
  }

  async login(){
    if(this.loginMail && this.loginPassword){
      await this.afAuth.auth.signInWithEmailAndPassword(this.loginMail, this.loginPassword).catch(error => {
        console.log(error);
     });
     let ref = await this.db.collection('users').doc(this.afAuth.auth.currentUser.uid).ref.get();
     let data = ref.data();
     if(!ref){
      await this.db.collection('users').doc(this.afAuth.auth.currentUser.uid).set({
        name: this.userName,
        email: this.loginMail,
        password: this.loginPassword
      });
     }
     this.navCtrl.setRoot(TabsPage);
    } else {
      alert("Bitte Logindaten eingeben");
    }
  }

  register(){
    this.navCtrl.push('RegisterPage', { registerMail: this.loginMail, registerPassword: this.loginPassword });
  }
}
