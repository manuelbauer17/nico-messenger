import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public userName: string = "";
  public registerMail: string = "";
  public registerPassword: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public db: AngularFirestore) {
    this.registerMail = this.navParams.get('registerMail');
    this.registerPassword = this.navParams.get('registerPassword');
  }

  async createUser(){
    if(this.registerMail && this.registerPassword){
      this.afAuth.auth.createUserWithEmailAndPassword(this.registerMail, this.registerPassword).then(success => {
        let id = success.user.uid;
        this.setIds(id);
        this.db.collection('users').doc(id).set({
          id: id,
          userName: this.userName,
          email: this.registerMail,
          chats: []
        });
        alert("Erfolgreich registriert");
      }).catch(error => {
        alert("Etwas ist schief gelaufen. Möglicherweise ist unter dieser E-Mail bereits ein Benutzer registriert");
      });
    } else {
      alert("Bitte gültige Daten eingeben");
    }
  }

  async setIds(id: any){
    let snapshot = await this.db.collection('meta').doc('allUsers').ref.get();
    if(snapshot){
      let idArray = snapshot.data() ? snapshot.data().ids : [];
      idArray.push(id);
      await this.db.collection('meta').doc('allUsers').set({ ids: idArray});
    }
  }
}
