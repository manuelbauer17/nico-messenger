import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { switchMap, map} from 'rxjs/operators'
import { BehaviorSubject, of, Observable } from 'rxjs';

/*
  Generated class for the ProvidersChatsChatsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChatsProvider {

  private chats$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private currentChatId: string = undefined;

  constructor(public db: AngularFirestore, public auth: AngularFireAuth) {
    this.auth.authState.pipe(switchMap((authState => {
      if (authState && authState.uid) {
        return this.db.collection('users').doc(this.auth.auth.currentUser.uid).valueChanges();
      } else {
        return of(null);
      }
    }))).subscribe(userData => {
      if (userData) {
        this.chats$.next(userData.chats);
      } else {
        this.chats$.next([]);
      }
    });
  }

  public getChat(id: string): Observable<any> {
    return this.chats$.pipe(map(chats => {
      let chatToReturn: any = null;
      chats.forEach(chat => {
        if (chat.chatId === id) {
          chatToReturn = chat;
        }
      });
      return chatToReturn;
    }));
  }


}
